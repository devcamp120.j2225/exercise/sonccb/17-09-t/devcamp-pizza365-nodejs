// khởi tạo bộ thư viện
const express = require('express');
// import controller
const { createNewOrder, getAllOrder, getOrderById, updateOrderById, deleteOrderById,getAllOrderOfUser } = require('../controller/orderController');
//khởi tạo router
const router = express.Router();

// create new order
router.post('/devcamp-pizza365/orders', createNewOrder);
// get all orders
router.get("/orders", getAllOrder);
//get all orders of user
router.get("/users/:userid/orders", getAllOrderOfUser);
// get order by id
router.get("/orders/:orderId", getOrderById);
//update order by id
router.put("/orders/:orderId", updateOrderById);
//delete order by id
router.delete("/orders/:orderId", deleteOrderById);

// export dữ liệu thành 1 module
module.exports = router